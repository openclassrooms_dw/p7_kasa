import React from 'react';
import ReactDOM from 'react-dom/client';
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import Home from './pages/Home';
import About from './pages/About';
import Error from './pages/Error';
import RentDescription from './pages/RentDescription'
import Footer from './layouts/Footer';
import Header from './layouts/Header';
import './styles/index.css';


const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
    errorElement: <Error />
  },
  {
    path: "/about",
    element: <About />
  },
  {
    path: "/rentDescription/:id",
    element: <RentDescription />,
    errorElement: <Error />
  },
  {
    path: "/error",
    element: <Error />
  }
])


const root = ReactDOM.createRoot(document.getElementById('root'))

root.render(
  <React.StrictMode>
      <Header/>
      <div className='pageContainer'>
      <main>
      <RouterProvider router={router} />
      </main>
      </div>
      <Footer/>
  </React.StrictMode>
)