import '../styles/home.css';
import RentList from '../components/RentList';
import Banner from '../components/Banner';
import banner from '../assets/banner_home.png'

function Home() {
  return (
    <>
    <div className="banner_home">
      <Banner src={banner} alt="Paysage océanique/forestière" text="Chez vous, partout et ailleurs"/>
    </div>
    <RentList/>
    </>
  );
}

export default Home;