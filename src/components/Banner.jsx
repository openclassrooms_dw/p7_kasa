import React from 'react';
import '../styles/banner.css'
import '../styles/about.css'

const Banner = ({text, src, alt}) => {
    return (
        <div className='banner_container'>
            <span className='banner_text'>{text}</span>
            <img src={src} alt={alt} className='banner_img'/>
        </div>
    );
};

export default Banner;